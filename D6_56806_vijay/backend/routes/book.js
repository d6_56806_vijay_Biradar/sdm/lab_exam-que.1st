const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/', (request, response) => {
  const { book_title,publisher_name,author_name } = request.body

  const query = `
    INSERT INTO book
      (book_title,publisher_name,author_name)
    VALUES
      ('${book_title}','${publisher_name}','${author_name}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/', (request, response) => {
  const query = `
    SELECT book_id, book_title,author_name
    FROM book
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/:id', (request, response) => {
  const { book_id } = request.params
  const { book_title } = request.body

  const query = `
    UPDATE book
    SET
      book_title = '${book_title}', 
    WHERE
      book_id = ${book_id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/:id', (request, response) => {
  const { book_id } = request.params

  const query = `
    DELETE FROM book
    WHERE
      book_id = ${book_id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
